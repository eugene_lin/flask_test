from flask import Flask, request, jsonify
import json
from flask_cors import CORS

app = Flask(__name__)
CORS(app, resources=r'/*')

count = 0

@app.route('/', methods=['GET', 'POST'])
def test():

    if request.method == 'GET':

        response = {
            "code": 1,
            "message": "Get successfully!!",
            "data": 'Hello World'
        }
        return jsonify(response)

    if request.method == 'POST':

        response = {
            "code": 1,
            "message": "Post successfully!!",
            "data": 'Hello World'
        }
        return jsonify(response)

if __name__ == '__main__':

    app.run(host="0.0.0.0")
